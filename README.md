# Test Shop

This is the test task (static site)

![alt text](info/screenshots/screen.png)

##Used stack
- webpack
- jQuery
- scss
- pug

##Installation process

```
git clone https://tripcher@bitbucket.org/tripcher/test-shop.git
npm install
npm run start
```