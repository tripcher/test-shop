const path = require('path');
const argv = require('yargs').argv;

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const isDevelopment = argv.mode === 'development';

module.exports = {
    entry: [
        './src/js/index.js',
    ],
    output: {
        filename: './js/bundle.js'
    },
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules\/*/,
                use: {
                    loader: 'babel-loader',
                }
            },
            {
                test: /\.pug$/,
                exclude: '/node_modules/',
                use: ['html-loader?attrs=false', 'pug-html-loader']
            },
            {
                test: /\.scss$/,
                use: [
                    isDevelopment ? 'style-loader' : MiniCssExtractPlugin.loader,
                    "css-loader",
                    "sass-loader"
                ]
            },
            {
                test: /\.(ttf|eot|woff|woff2)$/,
                use: {
                    loader: "file-loader",
                    options: {
                        name: "fonts/[name].[ext]",
                    },
                },
            },
            {
                test: /\.(png|jpg|gif|ico|svg)$/,
                use: {
                    loader: 'file-loader',
                    options: {
                        name: 'images/[name].[ext]',
                    }
                }
            }
        ]
    },
    plugins: [
        new CopyWebpackPlugin([{
            from: './media/images',
            to: './images'
        }]),
        !isDevelopment ? new ImageminPlugin({ test: /\.(jpe?g|png|gif|svg)$/i }) : () => {},
        new HtmlWebpackPlugin({
            filename: 'index.html',
            template: 'src/html/index.pug',
            inject: false
        }),
        new MiniCssExtractPlugin({
            filename: "style.css",
        })
    ]
};